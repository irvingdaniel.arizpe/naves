package com.microservicio.nave.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.microservicio.nave.entity.Naves;
import com.microservicio.nave.exceptions.GeneralServiceException;
import com.microservicio.nave.exceptions.NoDataFountException;
import com.microservicio.nave.exceptions.ValidateServiceException;
import com.microservicio.nave.repository.NavesRepository;
//import com.orderapi.exceptions.GeneralServiceException;
import com.microservicio.nave.validators.NavesValidator;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NaveService {

	@Autowired
	private NavesRepository naveRepo;
	
	
	public List<Naves> findAll(Pageable page){
		
		 try {
				List<Naves> naves = naveRepo.findAll(page).toList();
				return naves;

			} catch ( ValidateServiceException | NoDataFountException  e) {
				log.info(e.getMessage(), e);// error y excepcion
				throw e;//se lanza nuevamente el mensaje con un throw
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e.getMessage(), e);
				throw new com.microservicio.nave.exceptions.GeneralServiceException(e.getMessage(), e);
				//GeneralServiceException(e.getMessage(), e);
			}
		
	}
	
	
	public Naves findById(Long naveId) {
		try {
			log.debug("findById ==>"+naveId);
			Naves naves = naveRepo.findById(naveId)
					.orElseThrow(() -> new NoDataFountException("No existe la Nave"));
			return naves;
			
		}catch(ValidateServiceException | NoDataFountException e){
			log.info(e.getMessage(), e);// error y excepcion
			throw e;//se lanza nuevamente el mensaje con un throw
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
		
		
		
		
	}
	
	
	@Transactional
	public Naves save(Naves naves) {
		
		try {
			NavesValidator.save(naves);
			if(naves.getId()==null) {
				Naves newNaves = naveRepo.save(naves);
				return newNaves;
			}
			Naves exitnave=naveRepo.findById(naves.getId())
					.orElseThrow(() -> new NoDataFountException("No existe la nave"));
			exitnave.setName(naves.getName());
			naveRepo.save(exitnave);
			return exitnave;
			
			
		} catch (ValidateServiceException | NoDataFountException e) {
			log.info(e.getMessage(), e);
			throw e;
		}catch (Exception e) {
			log.info(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
		
		
	}
	
	@Transactional
	public void delete(Long naveId) {
		try {
			Naves nave = naveRepo.findById(naveId)
					.orElseThrow(() -> new NoDataFountException("No existe la nave"));
			naveRepo.delete(nave);
			
		}catch (ValidateServiceException | NoDataFountException e) {
			log.info(e.getMessage(), e);
			throw e;
		}catch (Exception e) {
			log.info(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
		
	}
	
	
}
