package com.microservicio.nave.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.microservicio.nave.converter.NavesConverter;
import com.microservicio.nave.dto.NavesDTO;
import com.microservicio.nave.entity.Naves;
import com.microservicio.nave.services.NaveService;
import com.microservicio.nave.utils.WrapperResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class NaveController {

	@Autowired
	private NaveService naveService;
	
	private NavesConverter converter = new NavesConverter();
	
	private List<Naves> naves =  new ArrayList<>(); 
	
	
  /*
  * peticion para invocar todas las naves
  * 
  * */
  
 @GetMapping(value="/naves")
 public ResponseEntity<List<NavesDTO>> findAll(
	 @RequestParam (value="pageNumber", required = false, defaultValue = "0") int pageNumber,
	 @RequestParam (value="pageZise", required= false, defaultValue = "10") int pageZise
	 ){
	 Pageable page = PageRequest.of(pageNumber, pageZise);
	 List<Naves> naves = naveService.findAll(page);
	 List<NavesDTO> dtonaves = converter.fromEntity(naves);
	 
	 return  new WrapperResponse(true, "succes", dtonaves)
     		.createResponse(HttpStatus.OK);
 }
 
 @GetMapping(value="/naves/{naveId}")
 public ResponseEntity<WrapperResponse<NavesDTO>> findById(@PathVariable("naveId") Long naveId){
	 Naves naves = naveService.findById(naveId);
	 NavesDTO naveDTO = converter.fromEntity(naves);
	 return new WrapperResponse<NavesDTO>(true, "succes", naveDTO)
			 .createResponse(HttpStatus.OK);
	 
 }
 
 
 @PostMapping(value="/naves")
 public ResponseEntity<NavesDTO> create(@RequestBody NavesDTO naves){
	 Naves newNaves = naveService.save(converter.fromDTO(naves));
	 NavesDTO navesDTO = converter.fromEntity(newNaves);
	 
	 return new WrapperResponse(true, "succes", navesDTO)
			 .createResponse(HttpStatus.CREATED);
	 
 }
 
 @PutMapping(value="/naves")
 public ResponseEntity<NavesDTO> update(@RequestBody NavesDTO nave){
	 Naves updatenave = naveService.save(converter.fromDTO(nave));
	 NavesDTO naveDTO = converter.fromEntity(updatenave);
	 return new WrapperResponse(true, "succes", naveDTO)
			 .createResponse(HttpStatus.OK);
 }
 
 
 @DeleteMapping(value="/naves/{naveId}")
 public ResponseEntity<?> delete(@PathVariable("naveId") Long naveId){
	 naveService.delete(naveId);
	 return new WrapperResponse(true, "succes", null)
			 .createResponse(HttpStatus.OK);
	 
 }
	
	
}
