package com.microservicio.nave.validators;

import com.microservicio.nave.entity.Naves;
import com.microservicio.nave.exceptions.ValidateServiceException;

public class NavesValidator {
	
	public static void save(Naves naves) {
		
		if(naves.getName() ==null || naves.getName().trim().isEmpty()) {
			throw new ValidateServiceException("El nombre es requerido");
		}
		
		if(naves.getName().length()>150) {
			throw new ValidateServiceException("El nombre admite 150 caracteres");
		}
		
	}

}
