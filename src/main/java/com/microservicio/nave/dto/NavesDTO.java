package com.microservicio.nave.dto;

import com.microservicio.nave.entity.Naves;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NavesDTO {
	
	private Long id;
	private String name;

}
