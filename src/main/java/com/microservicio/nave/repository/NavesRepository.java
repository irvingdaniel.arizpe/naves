package com.microservicio.nave.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.microservicio.nave.entity.Naves;
@Repository
public interface NavesRepository extends JpaRepository<Naves, Long> {

}
