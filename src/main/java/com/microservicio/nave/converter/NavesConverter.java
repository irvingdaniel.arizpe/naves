package com.microservicio.nave.converter;


import com.microservicio.nave.dto.NavesDTO;
import com.microservicio.nave.entity.Naves;



public class NavesConverter extends AbstractCoverter <Naves, NavesDTO> {

	@Override
	public NavesDTO fromEntity(Naves entity) {
		if(entity == null) return null;
		return NavesDTO.builder()
				.id(entity.getId())
				.name(entity.getName())
				.build();
	}

	@Override
	public Naves fromDTO(NavesDTO dto) {
		if(dto == null) return null;

		return Naves.builder()
				.id(dto.getId())
				.name(dto.getName())
				.build();
		
	}


	

}
